import React, {Component} from "react"
import "./form.css"

class Form extends Component{

  constructor(props){
    super(props)
    this.state = {
     daftarMobil : [
      {nama: "Brio", harga: 150000, Tipe: "Matic"},
      {nama: "HRV", harga: 400000, Tipe: "Manual"},
      {nama: "Mobilio", harga: 250000, Tipe: "Matic"}
    ],
     inputName : "",
     inputHarga : "",
     inputTipe : "",
     indexOfForm: -1    
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }
 
  handleDelete(event){
    let index = event.target.value
    let newDaftarMobil = this.state.daftarMobil
    let editedDataMobil = newDaftarMobil[this.state.indexOfForm]
    newDaftarMobil.splice(index, 1)

    if (editedDataMobil !== undefined){
      var newIndex = newDaftarMobil.findIndex((el) => el === editedDataMobil)
      this.setState({daftarMobil: newDaftarMobil, indexOfForm: newIndex})
      
    }else{
      
      this.setState({daftarMobil: newDaftarMobil})
    }
    
  }
  
  handleEdit(event){
    let index = event.target.value
    let dataMobil = this.state.daftarMobil[index]
    this.setState({
      inputName: dataMobil.nama,
      inputHarga: dataMobil.harga,
      inputTipe: dataMobil.Tipe,
      indexOfForm: index
    })
  }

  handleChange(event){
    let typeOfInput = event.target.name
    switch (typeOfInput){
      case "name":
      {
        this.setState({inputName: event.target.value});
        break
      }
      case "harga":
      {
        this.setState({inputHarga: event.target.value});
        break
      }
      case "Tipe":
      {
        this.setState({inputTipe: event.target.value});
          break
      }
    default:
      {break;}
    }
  }

  handleSubmit(event){
    event.preventDefault()

    let nama = this.state.inputName
    let harga = this.state.inputHarga.toString()
    let Tipe = this.state.inputTipe

    console.log(this.state)

    if (nama.replace(/\s/g,'') !== "" && harga.replace(/\s/g,'') !== ""){      
      let newDaftarMobil = this.state.daftarMobil
      let index = this.state.indexOfForm
      
      if (index === -1){
        newDaftarMobil = [...newDaftarMobil, {nama, harga, Tipe}]
      }else{
        newDaftarMobil[index] = {nama, harga, Tipe}
      }
  
      this.setState({
        daftarMobil: newDaftarMobil,
        inputName: "",
        inputHarga: "",
        inputTipe: ""
      })
    }

  }

  render(){
    return(
      <>
        <h1><span class="underline">Daftar Harga Mobil</span></h1>
        <table class="tabel">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Harga</th>
              <th>Tipe</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
              {
                this.state.daftarMobil.map((item, index)=>{
                  return(                    
                    <tr key={index}>
                      <td>{index+1}</td>
                      <td>{item.nama}</td>
                      <td>{item.harga}</td>
                      <td>{item.Tipe}</td>
                      <td>
                        <button onClick={this.handleEdit} value={index}>Edit</button>
                        &nbsp;
                        <button onClick={this.handleDelete} value={index}>Delete</button>
                      </td>
                    </tr>
                  )
                })
              }
          </tbody>
        </table>

        <h1><span class="underline">Form Daftar Harga Mobil</span></h1>
        <div style={{width: "50%", margin: "0 auto", display: "block"}}>
          <div style={{border: "2px solid #ed1a07", padding: "20px"}}>
            <form onSubmit={this.handleSubmit}>
              <label style={{float: "left"}}>
                Nama:
              </label>
              <input style={{float: "right", border: "1.5px solid #ed1a07"}} type="text" name="name" value={this.state.inputName} onChange={this.handleChange}/>
              <br/>
              <br/>
              <label style={{float: "left"}}>
                Harga:
              </label>
              <input style={{float: "right", border: "1.5px solid #ed1a07"}} type="text" name="harga" value={this.state.inputHarga} onChange={this.handleChange}/>
              <br/>
              <br/>
              <label style={{float: "left"}}>
                Tipe:
              </label>
              <input style={{float: "right", border: "1.5px solid #ed1a07"}} type="text" name="Tipe" value={this.state.inputTipe} onChange={this.handleChange}/>
              <br/>
              <br/>
              <div style={{width: "100%", paddingBottom: "20px"}}>
                <button style={{ float: "right"}}>Submit</button>
              </div>
            </form>
          </div>
        </div>
      </>
    )
  }
}

export default Form
